#!/usr/bin/env python3

"""
    PyNxHashTool
    Copyright (C) 2023  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# This is a Python port of https://github.com/IcySon55/NxHashTool

import sys
import os
import re
import zlib
import hashlib

# Some helper functions and classes


class crc32:
    name = "crc32"

    def __init__(self, data=b""):
        self.value = 0
        self.update(data)

    def hexdigest(self):
        return f"{self.value:08x}"

    def update(self, data):
        self.value = zlib.crc32(data, self.value)


def close(message):
    print(message)
    print("\n⚠ Exiting due to an error. ⚠")
    exit(1)


def help(error=False):
    print("\nUsage: pynxhash.py file.xci")
    if error:
        exit(1)
    exit(0)


def listhash(hashes):
    lines = []
    for key, value in hashes.items():
        lines.append(f"{key.upper()} {value.lower()}")
    return "\n".join(lines)


def hashfiles(filenames):
    # Generate hashes, with the option of stringing together multiple files for input.
    hashes = (
        crc32(),
        hashlib.md5(),
        hashlib.sha1(),
        hashlib.sha256(),
        hashlib.sha512(),
    )
    chunksize = 0x100000

    totalsize = 0
    counter = 0

    for name in filenames:
        totalsize += os.path.getsize(name)

    print("0%", end="")

    for name in filenames:
        with open(name, "rb") as f:
            while chunk := f.read(chunksize):
                for h in hashes:
                    h.update(chunk)
                counter += chunksize
                print(f"\r{counter/totalsize:.1%}", end="")
    print("\r", end="")

    results = {}

    for h in hashes:
        results[h.name] = h.hexdigest()

    return results


print("🐍 PyNxHash v1.0 🐍\n")

# Basic argument error-checking

if len(sys.argv) < 2:
    print("😐 An XCI file name was not provided.")
    help(True)

if len(sys.argv) > 2:
    print("😕 Too many file names.")
    help(True)

if "-h" in sys.argv or "--help" in sys.argv:
    help()

xci = sys.argv[1]

if not xci.endswith(".xci"):
    print("😕 This does not look like an XCI file. Is the .xci extension missing?")
    help(True)

gamenamere = re.search(r".*?(?=\] )\]", xci)

if not gamenamere:
    print("😖 The XCI file name is not in the NxDumpTool format.")
    help(True)

# Get XCI file name, without the extension

gamename = gamenamere.group()

# Get the full path the XCI file (without the extension), as well as the directory.

dirname = os.path.dirname(os.path.realpath(xci))

# Find the Initial Data file

pattern = r"^{} \(Initial Data\) \([0-9A-F]{{8}}\)\.bin$".format(
    gamename.replace("[", "\\[").replace("]", "\\]")
)

initialdata = ""

for line in os.listdir(path=dirname):
    if re.search(pattern, line):
        initialdata = line

if not initialdata:
    close("😖 The Initial Data BIN file does not exist.")

cardid = ""

for line in os.listdir(path=dirname):
    if re.search(pattern, line):
        cardid = line

pattern = r"^{} \(Card ID Set\) \([0-9A-F]{{8}}\)\.bin$".format(
    gamename.replace("[", "\\[").replace("]", "\\]")
)

if not cardid:
    close("😖 The Card ID BIN file does not exist.")

nullkeyarea = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "nullkeyarea.bin"
)
if not os.path.isfile(nullkeyarea):
    nullkeyarea = os.path.join(os.getcwd(), "nullkeyarea.bin")
if not os.path.isfile(nullkeyarea):
    close('😖 The "nullkeyarea.bin" file is missing. It should be 3584 bytes of zeros.')

# Generate file hashes

xcipath = os.path.realpath(xci)
idpath = os.path.realpath(initialdata)
cidpath = os.path.realpath(cardid)

xcout = f"{xcipath}.keyarealess.hashes.txt"
isout = f"{idpath}.hashes.txt"
ciout = f"{cidpath}.hashes.txt"
cmout = f"{xcipath}.keyarea.hashes.txt"

# XCI hashes

print("Step 1/4: Computing XCI hashes")
xcihash = hashfiles([xcipath])

with open(xcout, "w") as f:
    f.write(listhash(xcihash))
print("💾 Saved hashes")

# Initial Data hashes

print("Step 2/4: Computing Initial Data hashes")
idhash = hashfiles([idpath])

with open(isout, "w") as f:
    f.write(listhash(idhash))
print("💾 Saved hashes")

# Card ID hashes

print("Step 3/4: Computing Card ID hashes")
cidhash = hashfiles([cidpath])

with open(ciout, "w") as f:
    f.write(listhash(cidhash))
print("💾 Saved hashes")

# Full XCI hashes

print("Step 4/4: Computing full XCI hashes")
fullxcihash = hashfiles([idpath, nullkeyarea, xcipath])
temp = listhash(fullxcihash)

with open(cmout, "w") as f:
    f.write(temp)
print("💾 Saved hashes\n")

print("🎉 All steps completed 🎉")
