# PyNxHashTool

This is a Python 3 port of [IcySon55's NxHashTool](https://github.com/IcySon55/NxHashTool) with some minor modifications.

Just like the original, it combines multiple hashing methods into one step to simplify dumping.

# Changes

1. Linux support (Windows potentially works, but is untested)
2. Card ID dumps are now hashed

# Requirements

* Python 3
* [NxDumpTool Rewrite](https://github.com/DarkMatterCore/nxdumptool/) (Log in and check the Actions tab to download current builds)

# Usage

* Run the `nxdt_rw_proc` utility
  * Dump Initial data
  * Dump Card ID data
  * Dump XCI data
    * append key area: `no`
    * keep certificate: `no`
    * trim: `no`
    * calculate crc32: `yes`
* Compute hashes: `nxhashtool.py "path/to/file.xci"`


The end result will be 4 .txt files. The additonal file is for the Card ID.

Each file will contain CRC32, MD5, SHA1, SHA256, and SHA512 hashes.

